export const groupData = [
    { value: "1", label: 'IT Support' },
    { value: "2", label: 'Software Development' },
    { value: "3", label: 'Network Operations' },
    { value: "4", label: 'Data Science' },
    { value: "5", label: 'Quality Assurance' },
    { value: "6", label: 'Project Management' },
    { value: "7", label: 'Infrastructure' },
    { value: "8", label: 'Web Development' },
    { value: "9", label: 'Database Administration' },
    { value: "10", label: 'Cybersecurity' },
];