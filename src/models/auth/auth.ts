export interface SignInRequest {
    username: string
    password: string
}

export interface SignInResponse {
    state: boolean
    message: string
    data?: SignInData
}

export interface SignInData {
    fullName: string
    key: string
    type: string
}

