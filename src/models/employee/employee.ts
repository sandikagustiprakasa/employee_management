

export interface EmployeeRequest {
    page: number
    perPage: number
    searchText: string
    sortByName: number
}

export interface EmployeeResponse {
    state: boolean
    message: string
    data?: EmployeeData[] | null
    totalRecord: number
}

export interface EmployeeDetailRequest {
    username: string
}

export interface EmployeeDetailResponse {
    state: boolean
    message: string
    data?: EmployeeData | null
}

export interface EmployeeData {
    username: string
    firstName: string
    lastName: string
    email: string
    birthDate: string
    basicSalary: number
    status: number
    group_id: number
    description: string
    groupName: string
}

export interface EmployeeDeleteRequest {
    username: string
    status: number
}

export interface EmployeeDeleteResponse {
    state: boolean
    message: string
}

export interface EmployeeInsertRequest {
    username: string
    firstName: string
    lastName: string
    email: string
    birthDate: string
    basicSalary: number
    groupID: number
    description: string
}

export interface EmployeeInsertResponse {
    state: boolean
    message: string
}


export interface EmployeeUpdateRequest {
    username: string
    firstName: string
    lastName: string
    email: string
    birthDate: string
    basicSalary: number
    groupID: number
    description: string
}

export interface EmployeeUpdateResponse {
    state: boolean
    message: string
}