var width = 100,
    perfData = window.PerformanceNavigationTiming, // The PerformanceTiming interface represents timing-related performance information for the given page.
    EstimatedTime = -(perfData.loadEventEnd - perfData.navigationStart),
    time = parseInt((EstimatedTime/1000)%60)*100;

// Percentage Increment Animation
var percentageID = $("#precent"),
    progressID = $("#progress"),
    start = 0,
    end = 100,
    durataion = time;
    animateValue(percentageID,progressID, start, end, durataion);

function animateValue(percent,progress, start, end, duration) {

  var range = end - start,
      current = start,
      increment = end > start? 1 : -1,
      stepTime = Math.abs(Math.floor(duration / range)),
      persentObj = $(percent),
      progressObj = $(progress);

  var timer = setInterval(function() {
    current += increment;
    $(persentObj).html(current + "%");
    $(progressObj).css('width', current + '%');
      //obj.innerHTML = current;
    if (current == end) {
      setTimeout(() => {
        $('#loading').remove();
      }, 500);
      clearInterval(timer);
    }
  }, stepTime);
}
