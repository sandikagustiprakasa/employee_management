import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Subject, Subscription, take, takeUntil } from 'rxjs';
import { EmployeeData, EmployeeDeleteRequest, EmployeeDeleteResponse, EmployeeRequest, EmployeeResponse } from 'src/models/employee/employee';
import { AuthService } from 'src/services/auth/authorization.service';
import { EmployeeService } from 'src/services/employee/employee.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit, OnDestroy {

  public fullName = this.authService.getFullName();

  public employeeRequest: EmployeeRequest = {
    page: 1,
    perPage: 10,
    searchText: "",
    sortByName: 0
  };
  public batchDetailMaxPage: number = 1;
  public batchDetailMaxPageArray: number[] = Array.from({ length: this.batchDetailMaxPage });
  public employees?: EmployeeData[] | null = [];
  public employeeTotal: number = 0;
  public employeeSubscription?: Subscription;
  public employeeLoading: boolean = false;
  public employeeEmpty: boolean = false;
  public employeeEmptyMessage: string = "";

  public onDestroy$ = new Subject<void>();

  constructor(
    public router: Router,
    private authService: AuthService,
    private employeeService: EmployeeService,
    private message: NzMessageService,
  ) {}
  
  ngOnInit(): void {
    this.employeeService.storingListRequest$
    .pipe(
      takeUntil(this.onDestroy$)
    )
    .subscribe({
      next: (value: EmployeeRequest|null) => {
        if(value){
          this.employeeRequest = value; 
        }
      }
    })
    this.cssOverWrite();
    this.getEmployeeList();
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  onClickSignOut(){
    this.authService.clearAuthLocalStorage();
    this.authService.clearAuthSubject();
    this.router.navigate(['/auth']);
  }

  onChangePerPage(){
    this.employeeRequest.page = 1;
    this.getEmployeeList();
  }

  onChangeSort(){
    this.getEmployeeList();
  }

  onKeyUpSearch(){
    if(this.employeeSubscription){
      this.employeeSubscription.unsubscribe();
    }
    this.employeeRequest.page = 1;
    this.getEmployeeList();
  }

  onClickRefreshList(){
    this.employeeRequest.page = 1;
    this.employeeRequest.perPage = 10;
    this.employeeRequest.searchText = "";
    this.employeeRequest.sortByName = 0;
    this.getEmployeeList();
  }

  getEmployeeList(){
    this.employeeLoading = true;
    this.employeeEmpty = false;
    this.employeeEmptyMessage = "";
    if(this.employeeSubscription){
      this.employeeSubscription.unsubscribe();
    }
    this.employeeSubscription = this.employeeService.list(this.employeeRequest).pipe(
      take(1)
    ).subscribe({
      next: (result: EmployeeResponse) =>{
        this.employeeLoading = false;
        if(result?.state){
          this.employeeService.setStoringListRequest(this.employeeRequest);
          this.employees = result.data;
          this.employeeTotal = result.totalRecord;
          this.batchDetailMaxPage = Math.ceil(result.totalRecord / this.employeeRequest.perPage);
          this.batchDetailMaxPageArray = Array.from({ length: this.batchDetailMaxPage });
        }else{
          this.employees = [];
          this.employeeTotal = 0;
          this.employeeEmpty = true;
          this.employeeEmptyMessage =  result.message;
          this.batchDetailMaxPage = 1;
          this.batchDetailMaxPageArray = Array.from({ length: this.batchDetailMaxPage });
        }
      }
    })
  }

  goToPaginationBatchDetail(pageNumber: number) {
      if (pageNumber === this.employeeRequest.page) {
          return;
      }
      this.employeeRequest.page = pageNumber;
      this.getEmployeeList();
  }

  nextPaginationBatchDetail() {
      this.employeeRequest.page += 1;
      if (this.employeeRequest.page <= this.batchDetailMaxPage) {
          this.getEmployeeList();
      } else {
          this.employeeRequest.page = this.batchDetailMaxPage;
      }
  }

  previousPaginationBatchDetail() {
      this.employeeRequest.page -= 1;
      if (this.employeeRequest.page > 0) {
          this.getEmployeeList();
      } else {
          this.employeeRequest.page = 1;
      }
  }

  onConfirmDelete(index: number){
    let request : EmployeeDeleteRequest = {
      username: this.employees![index].username,
      status: 0
    }
    this.employeeService.delete(request).pipe(
      take(1)
    ).subscribe({
      next: (result: EmployeeDeleteResponse) =>{
        if(result?.state){
          this.getEmployeeList();
        }else{
          this.message.create('error', result.message);
        }
      }
    })
  }

  onClickAddNew(){
    this.router.navigate(['/employee/add']);
  }

  onClickEdit(index: number){
    this.router.navigate(['/employee/edit/' + this.employees![index].username]);
  }

  onClickDetail(index: number){
    this.router.navigate(['/employee/view/' + this.employees![index].username]);
  }

  cssOverWrite(){
    $('body').css('background-color','#FFFFFF');
    $('body').css('background-image','none');
  }
}
