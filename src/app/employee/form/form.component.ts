import { Component, ElementRef, OnInit, QueryList, ViewChildren } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { take } from 'rxjs';
import { EmployeeService } from 'src/services/employee/employee.service';
import { 
    EmployeeDetailRequest, 
    EmployeeDetailResponse, 
    EmployeeInsertRequest, 
    EmployeeInsertResponse, 
    EmployeeUpdateRequest, 
    EmployeeUpdateResponse
} from 'src/models/employee/employee';
import { Select2Data } from 'ng-select2-component';
import { groupData } from 'src/models/group/group';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  @ViewChildren('required') requiredFrom! : QueryList<ElementRef>;

  readonly VIEW_PATH = "view";
  readonly EDIT_PATH = "edit";

  public usernameQueryString: string | null= "";
  public pathName: string = "";

  public username: string = "";
  public firstName: string = "";
  public lastName: string = "";
  public email: string = "";
  public birthDate: Date | null = null;
  public birthDateString: string = "";
  public basicSalary: number = 0;
  public groupID: number = 0;
  public description: string = "";
  public groupName: string = "";

  public disabledButton = false;
  public todayDate: string = "";

  public dataGroup: Select2Data = groupData;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private employeeService: EmployeeService,
    private message: NzMessageService,
  ) {}

  ngOnInit(): void {
    this.usernameQueryString = this.route.snapshot.paramMap.get('username');
    this.pathName = this.route.snapshot.url[0].path;
    if(this.usernameQueryString){
      this.getDetailEmployee();
    }
    this.todayDate = this.getCurrentDateFormatted();
  }

  getDetailEmployee(){
    this.disabledButton = true;
    let loading = this.message.loading('Getting Employee..').messageId;
    let request : EmployeeDetailRequest = {
      username : this.usernameQueryString!
    }
    this.employeeService.detail(request).pipe(
      take(1)
    ).subscribe({
      next: (result: EmployeeDetailResponse) =>{
        this.message.remove(loading);
        if(result?.state){
          this.username = result.data!.username;
          this.firstName = result.data!.firstName;
          this.lastName = result.data!.lastName;
          this.email = result.data!.email;
          this.birthDateString = result.data!.birthDate;
          this.birthDate = new Date(result.data!.birthDate);
          this.basicSalary = result.data!.basicSalary;
          this.groupID = result.data!.group_id;
          this.description = result.data!.description;
          this.groupName = result.data!.groupName;
          this.disabledButton = false;
        }else{
          this.message.error(result.message, {nzDuration:1000}).onClose!.subscribe({next: () => this.router.navigate(['/employee']) });
        }
      }
    })
  }

  saveNewEmployee(){
    let loading = this.message.loading('Creating new Employee').messageId;
    let request : EmployeeInsertRequest = {
      username : this.username,
      firstName : this.firstName,
      lastName : this.lastName,
      email : this.email,
      birthDate : this.getCurrentDateFormatted(this.birthDate),
      basicSalary : this.basicSalary,
      groupID : this.groupID,
      description : this.description
    }
    this.employeeService.insert(request).pipe(
      take(1)
    ).subscribe({
      next: (result: EmployeeInsertResponse) =>{
        this.message.remove(loading);
        if(result?.state){
          this.message.success("Create new success", {nzDuration:1000}).onClose!.subscribe({next: () => this.router.navigate(['/employee/view/' + request.username]) });
        }else{
          this.message.create('error', result.message);
        }
        this.disabledButton = false;
      }
    })
  }

  editEmployee(){
    let loading = this.message.loading('Editing Employee').messageId;
    let request : EmployeeUpdateRequest = {
      username : this.username,
      firstName : this.firstName,
      lastName : this.lastName,
      email : this.email,
      birthDate : this.getCurrentDateFormatted(this.birthDate),
      basicSalary : this.basicSalary,
      groupID : this.groupID,
      description : this.description
    }
    this.employeeService.update(request).pipe(
      take(1)
    ).subscribe({
      next: (result: EmployeeUpdateResponse) =>{
        this.message.remove(loading);
        if(result?.state){
          this.message.success("Edit success", {nzDuration:1000}).onClose!.subscribe({next: () => this.router.navigate(['/employee/view/' + request.username]) });
        }else{
          this.message.create('error', result.message);
        }
        this.disabledButton = false;
      }
    })
  }

  onClickCancel(){
    if(this.disabledButton){
      return;
    }
    this.router.navigate(['/employee']);
  }

  onClickSave(){
    if(this.disabledButton){
      return;
    }
    this.disabledButton = true;

    let requiredForm = true;
    for(let i = 0; i < this.requiredFrom["_results"].length; i++){
      let element = this.requiredFrom["_results"][i];
      if(element.nativeElement.value == ""){
        requiredForm = false;
        break;
      }
    }
    
    if(!requiredForm){
      this.message.create('warning', "Semua form harus diisi");
      this.disabledButton = false;
      return;
    }

    if(this.pathName !== this.EDIT_PATH){
      this.saveNewEmployee();
    }else{
      this.editEmployee();
    }
  }

  getCurrentDateFormatted(dateParam: Date | null = null) {
    const currentDate = dateParam? new Date(dateParam) : new Date();
    const year = currentDate.getFullYear();
    const month = String(currentDate.getMonth() + 1).padStart(2, '0');
    const day = String(currentDate.getDate()).padStart(2, '0');
    const formattedDate = `${year}-${month}-${day}`;
    return formattedDate;
  }

  formatCurrency(event: any) {
    let input = event.target.value;
    let numericValue: number | null = 0;
    numericValue = parseFloat(input.replace(/[^0-9.]/g, ''));
    if(isNaN(numericValue)){
      numericValue = 0;
    }
    this.basicSalary = numericValue;
  }

  validateEmail() {
    let regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if (!regex.test(this.email)) {
      this.message.create('error', "Invalid E-mail format");
      this.email = "";
    }
  }
}
