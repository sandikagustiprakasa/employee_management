import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeRoutingModule } from './employee-routing.module';
import { ListComponent } from './list/list.component';
import { FormComponent } from './form/form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzModule } from 'src/modules/antdesign/antdesign.module';
import { RupiahPipe } from '../pipes/rupiah.pipe';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from '@danielmoncada/angular-datetime-picker';
import { Select2Module } from 'ng-select2-component';

@NgModule({
  declarations: [
    ListComponent,
    FormComponent,
    RupiahPipe
  ],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NzModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    Select2Module
  ],
})
export class EmployeeModule { }
