import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from './list/list.component';
import { FormComponent } from './form/form.component';
import { GuardService } from 'src/services/auth/guard.service';

const routes : Routes = [
  {
    path: '',
    component: ListComponent,
    canActivate: [GuardService]
  },
  {
    path: 'add',
    component: FormComponent,
    canActivate: [GuardService]
  },
  {
    path: 'edit/:username',
    component: FormComponent,
    canActivate: [GuardService]
  },
  {
    path: 'view/:username',
    component: FormComponent,
    canActivate: [GuardService]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
