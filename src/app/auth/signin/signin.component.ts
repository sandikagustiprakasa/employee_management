import { Component, ElementRef, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { SignInRequest, SignInResponse } from 'src/models/auth/auth';
import { AuthService } from 'src/services/auth/authorization.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  @ViewChildren('required') requiredFrom! : QueryList<ElementRef>;
  
  public username : string = "sandika.prakasa";
  public password : string = "abc123";
  public isLoading : boolean = false;

  constructor(
    public router : Router,
    private message: NzMessageService,
    private authService : AuthService,
  ) {}
  
  ngOnInit(): void {
    this.cssOverWrite();
  }

  enterKeyUp() {
    this.onClickLogin();
  }

  onClickLogin(){
    
    if(this.isLoading){ return; }

    this.isLoading = true;
    
    let requiredForm = true;
    for(let i = 0; i < this.requiredFrom["_results"].length; i++){
      let element = this.requiredFrom["_results"][i];
      if(element.nativeElement.value == ""){
        requiredForm = false;
        break;
      }
    }
    
    if(!requiredForm){
      this.message.create('warning', "Username dan Password tidak boleh kosong");
      this.isLoading = false;
      return;
    }
    
    let request: SignInRequest = {
      username: this.username,
      password: this.password
    }
    this.authService.signIn(request).subscribe({
      next : (result : SignInResponse)=>{
        if(result.state){
          this.router.navigate(['/employee']);
        }else{
          this.message.create('error', result.message);
          this.isLoading = false;
        }
      }
    });
  }

  cssOverWrite(){
    $('body').css('background','#043A36');
    $('body')
      .css('background-image', 'url("../../../assets/img/login-background.png")')
      .css('background-repeat', 'no-repeat')
      .css('background-attachment', 'fixed');
    $('body').css('background-size','cover !important');
  }
}
