import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, of } from 'rxjs';
import { environment } from '../../env/environment';
import { AuthService } from '../auth/authorization.service';
import { 
  EmployeeDeleteRequest, 
  EmployeeDeleteResponse, 
  EmployeeDetailRequest, 
  EmployeeDetailResponse, 
  EmployeeInsertRequest, 
  EmployeeInsertResponse, 
  EmployeeRequest, 
  EmployeeResponse,
  EmployeeUpdateRequest,
  EmployeeUpdateResponse
} from 'src/models/employee/employee';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  
  public storingListRequest$ = new BehaviorSubject<EmployeeRequest|null>(null);

  constructor(
    private http : HttpClient,
    private authService : AuthService,
  ) { }

  list(request : EmployeeRequest) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.authService.getToken()
    });
    let options = { headers: headers };
    return this.http.post<EmployeeResponse>(API_URL +'/?section=employee&action=list', request, options).pipe(
      catchError((err) => {
        let errorReturn : EmployeeResponse = {
          state: false,
          message: (err?.message)? err.message : JSON.stringify(err.message),
          data: null,
          totalRecord: 0
        }
        return of(errorReturn); 
      })
    );
  }

  detail(request : EmployeeDetailRequest) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.authService.getToken()
    });
    let options = { headers: headers };
    return this.http.post<EmployeeDetailResponse>(API_URL +'/?section=employee&action=detail', request, options).pipe(
      catchError((err) => {
        let errorReturn : EmployeeDetailResponse = {
          state: false,
          message: (err?.message)? err.message : JSON.stringify(err.message),
          data: null
        }
        return of(errorReturn); 
      })
    );
  }

  insert(request : EmployeeInsertRequest) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.authService.getToken()
    });
    let options = { headers: headers };
    return this.http.post<EmployeeInsertResponse>(API_URL +'/?section=employee&action=addnew', request, options).pipe(
      catchError((err) => {
        let errorReturn : EmployeeInsertResponse = {
          state: false,
          message: (err?.message)? err.message : JSON.stringify(err.message)
        }
        return of(errorReturn); 
      })
    );
  }

  update(request : EmployeeUpdateRequest) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.authService.getToken()
    });
    let options = { headers: headers };
    return this.http.post<EmployeeUpdateResponse>(API_URL +'/?section=employee&action=edit', request, options).pipe(
      catchError((err) => {
        let errorReturn : EmployeeUpdateResponse = {
          state: false,
          message: (err?.message)? err.message : JSON.stringify(err.message)
        }
        return of(errorReturn); 
      })
    );
  }


  delete(request : EmployeeDeleteRequest) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.authService.getToken()
    });
    let options = { headers: headers };
    return this.http.post<EmployeeDeleteResponse>(API_URL +'/?section=employee&action=softdeleteorrestore', request, options).pipe(
      catchError((err) => {
        let errorReturn : EmployeeDeleteResponse = {
          state: false,
          message: (err?.message)? err.message : JSON.stringify(err.message)
        }
        return of(errorReturn); 
      })
    );
  }

  setStoringListRequest(value: EmployeeRequest){
    this.storingListRequest$.next(value);
  }

  getStoringListRequest(){
    return (this.storingListRequest$).value;
  }

}
