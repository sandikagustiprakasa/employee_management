import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, of, tap } from 'rxjs';
import { environment } from '../../env/environment';
import { CryptoService } from '../crypto/crypto.service';
import { SignInData, SignInRequest, SignInResponse } from 'src/models/auth/auth';

const API_URL = environment.apiUrl;
const LOCAL_STORAGE = environment.localStorage;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  public authData$ = new BehaviorSubject<SignInData|null>(null);

  constructor(
    private http : HttpClient,
    private cryptoService : CryptoService,
  ) { }

  signIn(request : SignInRequest) {

    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    let options = { headers: headers };

    return this.http.post<SignInResponse>(API_URL +'/?section=auth&action=signin', request, options).pipe(
      tap((result : SignInResponse) => {
        if(result?.state){
          this.setAuthLocalStorage(result);
          this.storingLocalStorageToSubject();
        }
      }),
      catchError((err) => {
        let errorReturn : SignInResponse = {
          state: false,
          message: (err?.message)? err.message : JSON.stringify(err.message)
        }
        return of(errorReturn); 
      })
    );
  }

  setAuthLocalStorage(request : SignInResponse){
    localStorage.setItem(
      this.cryptoService.md5(LOCAL_STORAGE.authData),
      this.cryptoService.set(JSON.stringify(request.data))
    );
  }

  storingLocalStorageToSubject(){
    let authData : string | null = localStorage.getItem(this.cryptoService.md5(LOCAL_STORAGE.authData));
    if(authData){
      this.authData$.next(JSON.parse(this.cryptoService.get(authData!)));
    }else{
      this.authData$.next(null);
    }
  }

  clearAuthSubject(){
    localStorage.removeItem(this.cryptoService.md5(LOCAL_STORAGE.authData));
  }

  clearAuthLocalStorage(){
    this.authData$.next(null);
  }

  getToken(){
    let authData : SignInData | null = this.authData$.value;
    return  authData?.type + " " + authData?.key
  }

  getFullName(){
    let authData : SignInData | null = this.authData$.value;
    return  authData?.fullName
  }

}
