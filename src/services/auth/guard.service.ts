
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './authorization.service';

@Injectable({
  providedIn: 'root'
})
export class GuardService {

    constructor(
    private router: Router,
    private authService : AuthService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
        this.authService.storingLocalStorageToSubject();
        return new Promise(async (resolve) => {
            let authData = this.authService.authData$.value;
            if (!authData) {
                this.router.navigate(['/auth']);
                resolve(false);
            }
            resolve(true);
        });
    }
}
