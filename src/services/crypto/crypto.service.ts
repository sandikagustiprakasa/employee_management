import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { environment } from '../../env/environment';

const ENCRYPT_DECRYPT = environment.encryptDecrypt;

@Injectable({
  providedIn: 'root'
})
export class CryptoService {

  constructor() { }

  //The set method is use for encrypt the value.
  set(value : string){
    var key = CryptoJS.enc.Utf8.parse(ENCRYPT_DECRYPT.key);
    var iv = CryptoJS.enc.Utf8.parse(ENCRYPT_DECRYPT.key);
    var encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(value.toString()), key,
    {
        keySize: 128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });

    return encrypted.toString();
  }

  //The get method is use for decrypt the value.
  get(value : string){
    var key = CryptoJS.enc.Utf8.parse(ENCRYPT_DECRYPT.key);
    var iv = CryptoJS.enc.Utf8.parse(ENCRYPT_DECRYPT.key);
    var decrypted = CryptoJS.AES.decrypt(value, key, {
        keySize: 128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });

    return decrypted.toString(CryptoJS.enc.Utf8);
  }

  md5(value : string) {
    const hash = CryptoJS.MD5(CryptoJS.enc.Latin1.parse(value));
    const md5 = hash.toString(CryptoJS.enc.Hex)
    return md5;
  }
}
