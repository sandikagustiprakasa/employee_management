<?php
require_once 'config/connection.php';
require_once 'config/authorization.php';
require_once 'config/allowed-request.php';
switch ($_GET['section'] ?? null) {
    case "auth":
        require_once 'route/authRouter.php';
        break;
    case "employee":
        require_once 'route/employeeRouter.php';
        break;
    default:
        header("Location: /?section=auth&action=signin", true, 301);
        exit;
}

