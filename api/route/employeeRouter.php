<?php
require_once 'controller/employeeController.php';
$controller = new employeeController();
switch ($_GET['action'] ?? null) {
    case "list":
        echo json_encode($controller->list());
        break;
    case "detail":
        echo json_encode($controller->detail());
        break;
    case "addnew":
        echo json_encode($controller->addNew());
        break;
    case "edit":
        echo json_encode($controller->edit());
        break;
    case "softdeleteorrestore":
        echo json_encode($controller->softDeleteOrRestore());
        break;
    default:
        header("Location: ./", true, 301);
        exit;
}