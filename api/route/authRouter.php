<?php
require_once 'controller/authController.php';
$controller = new authController();
switch ($_GET['action'] ?? null) {
    case "signin":
        echo json_encode($controller->signIn());
        break;
    default:
        header("Location: ./", true, 301);
        exit;
}