<?php

class connection
{
    const serverName = "localhost";
    const username = "root";
    const password = "";
    const database = "employee_management_db";

    public static function start()
    {
        $conn = new mysqli(
            SELF::serverName,
            SELF::username,
            SELF::password,
            SELF::database
        );
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        return $conn;
    }
}
