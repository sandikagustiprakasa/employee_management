<?php

class allowedRequest
{

    public $method;

    public function __construct($method)
    {
        $this->method = $method;
    }

    public function do()
    {
        if(isset($this->method['POST'])){
            $action = $this->method['POST'];
            if(in_array($_GET['action'], $action)){
                if ($_SERVER["REQUEST_METHOD"] !== "POST") {
                    header('HTTP/1.0 405 Method Not Allowed');
                    echo json_encode(['message' => 'Method not allowed']);
                    exit;
                }
            }
        }
    }
}
