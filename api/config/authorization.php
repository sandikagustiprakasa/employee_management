<?php

class authorization
{

    public $except;

    public function __construct($except = null)
    {
        $this->except = $except;
    }

    public function do()
    {
        if (!isset($this->except)) {
            return false;
        }
        if (!in_array($_GET['action'], $this->except)) {
            if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])) {
                header('WWW-Authenticate: Basic realm="Authentication Required"');
                header('HTTP/1.0 401 Unauthorized');
                echo json_encode(['message' => 'Authentication Required']);
                exit;
            }

            $conn = connection::start();
            $sql = "SELECT count(*) AS count FROM ms_user_login WHERE username = '" . $_SERVER['PHP_AUTH_USER'] . "' AND password = '" . md5($_SERVER['PHP_AUTH_PW']) . "'";
            $result = $conn->query($sql);
            if (!$result) {
                echo json_encode(['message' => "Error: " . $conn->error]);
                exit;
            }

            $row = $result->fetch_assoc();
            $count = $row['count'];
            $conn->close();
            if ($count < 1) {
                header('WWW-Authenticate: Basic realm="Authentication Required"');
                header('HTTP/1.0 401 Unauthorized');
                echo json_encode(['message' => 'Invalid credentials']);
                exit;
            }
        }
    }
}
