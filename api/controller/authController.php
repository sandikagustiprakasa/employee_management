<?php

class authController extends connection
{

    public function __construct()
    {
        $except = ['signin'];
        $auth = new authorization($except);
        $auth->do();

        $method = [
            'POST' => ['signin']
        ];
        $methodAllowed = new allowedRequest($method);
        $methodAllowed->do();
    }

    public function signIn()
    {
        $data = null;
        $conn = SELF::start();
        try {
            $jsonData = file_get_contents("php://input");
            $decodedData = json_decode($jsonData);

            if (!isset($decodedData->username) || !isset($decodedData->password)) {
                throw new Exception("Error: Bad request");
            }
            $sql = "SELECT employee.firstName, employee.lastName FROM ms_user_login userLogin
                    JOIN ms_employee employee ON employee.username = userLogin.username
                    WHERE 
                        userLogin.username = '" . $decodedData->username . "' AND 
                        userLogin.password = '" . md5($decodedData->password) . "'
                    ";
            $result = $conn->query($sql);
            if (!$result) {
                throw new Exception("Error: " . $conn->error);
            }
            $row = $result->fetch_assoc();
            if (!$row) {
                throw new Exception("Invalid credential");
            }
            $conn->close();
            return array(
                'state' => true,
                'message' => 'Ok',
                'data' => [
                    'fullName' => ($row['firstName'] ?? '') . " " . ($row['lastName'] ?? ''),
                    'key' => base64_encode("$decodedData->username:$decodedData->password"),
                    'type' => 'Basic'
                ]
            );
        } catch (Exception $ex) {
            $conn->close();
            return array(
                'state' => false,
                'message' => $ex->getMessage(),
                'data' => $data
            );
        }
    }
}
