<?php

class employeeController extends connection
{

    public function __construct()
    {
        $auth = new authorization([]);
        $auth->do();

        $method = [
            'POST' => [
                'list',
                'addnew',
                'edit',
                'softdeleteorrestore'
            ]
        ];
        $methodAllowed = new allowedRequest($method);
        $methodAllowed->do();
    }

    public function list()
    {
        $data = null;
        $totalRecord = null;
        $conn = SELF::start();
        try {
            $jsonData = file_get_contents("php://input");
            $decodedData = json_decode($jsonData);

            $startData = null;
            $endData = null;
            $page = $decodedData->page ?? null;
            $perPage = $decodedData->perPage ?? null;
            $searchText = $decodedData->searchText ?? null;
            $sortByName = $decodedData->sortByName ?? null;
            
            if (is_numeric($page) && is_numeric($perPage)) {
                $endData = $perPage;
                $startData = ($perPage * $page) - $perPage;
            }

            $sqlCount = "SELECT COUNT(*) as count FROM ms_employee employee
                            JOIN lk_group lkgroup ON lkgroup.id=employee.group_id
                            WHERE 
                                (employee.firstName LIKE '%" . $searchText . "%' OR 
                                employee.lastName LIKE '%" . $searchText . "%')
                            AND
                                employee.status = 1
                        ";
            $resultCount = $conn->query($sqlCount);
            if (!$resultCount) {
                throw new Exception("Error: " . $conn->error);
            }
            $rowCount = $resultCount->fetch_assoc();
            $totalRecord = intval($rowCount['count'] ?? 0);

            $sql = "SELECT employee.*, lkgroup.description AS groupName FROM ms_employee employee
                    JOIN lk_group lkgroup ON lkgroup.id=employee.group_id
                    WHERE 
                        (employee.firstName LIKE '%" . $searchText . "%' OR 
                        employee.lastName LIKE '%" . $searchText . "%')
                    AND
                        employee.status = 1
                    ";
            $sql = $sql . " ORDER BY employee.firstName ". (($sortByName == 0)? "ASC" : "DESC");
            $sql = $sql . " LIMIT " . ($startData ?? 0) . "," . ($endData ?? $totalRecord);

            $result = $conn->query($sql);
            if (!$result) {
                throw new Exception("Error: " . $conn->error);
            }
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
            if (!is_array($data) || count($data) < 1) {
                throw new Exception("There is no data");
            }
            $conn->close();
            return array(
                'state' => true,
                'message' => 'Ok',
                'data' => $data,
                'totalRecord' => $totalRecord
            );
        } catch (Exception $ex) {
            $conn->close();
            return array(
                'state' => false,
                'message' => $ex->getMessage(),
                'data' => $data,
                'totalRecord' => $totalRecord
            );
        }
    }

    public function detail()
    {
        $data = null;
        $conn = SELF::start();
        try {
            $jsonData = file_get_contents("php://input");
            $decodedData = json_decode($jsonData);

            $username = $decodedData->username ?? null;
            if(!isset($username)){
                throw new Exception("Bad request");
            }
            
            $sql = "SELECT employee.*, lkgroup.description AS groupName FROM ms_employee employee
                    JOIN lk_group lkgroup ON lkgroup.id=employee.group_id
                    WHERE 
                        employee.username = '". $username ."'
                    AND
                        employee.status = 1
                    LIMIT 0,1
                    ";
            $result = $conn->query($sql);
            if (!$result) {
                throw new Exception("Error: " . $conn->error);
            }
            while ($row = $result->fetch_assoc()) {
                $data = $row;
            }
            if (!isset($data)) {
                throw new Exception("There is no data");
            }
            $conn->close();
            return array(
                'state' => true,
                'message' => 'Ok',
                'data' => $data
            );
        } catch (Exception $ex) {
            $conn->close();
            return array(
                'state' => false,
                'message' => $ex->getMessage(),
                'data' => $data
            );
        }
    }

    public function addNew()
    {
        $conn = SELF::start();
        try {
            $jsonData = file_get_contents("php://input");
            $decodedData = json_decode($jsonData);

            $username = $decodedData->username ?? null;
            $firstName = $decodedData->firstName ?? null;
            $lastName = $decodedData->lastName ?? null;
            $email = $decodedData->email ?? null;
            $birthDate = $decodedData->birthDate ?? null;
            $basicSalary = $decodedData->basicSalary ?? null;
            $group_id = $decodedData->groupID ?? null;
            $description = $decodedData->description ?? null;

            $sqlInsert = "INSERT INTO ms_employee (username, firstName, lastName, email, birthDate, basicSalary, status, group_id, description) 
                            VALUES ('$username' , '$firstName' , '$lastName' , '$email' , '$birthDate' , '$basicSalary' , '1' , '$group_id' , '$description')";
            $resultInsert = $conn->query($sqlInsert);
            if (!$resultInsert) {
                throw new Exception("Error: " . $conn->error);
            }
            $conn->close();
            return array(
                'state' => true,
                'message' => 'Insert new data Ok'
            );
        } catch (Exception $ex) {
            $conn->close();
            return array(
                'state' => false,
                'message' => $ex->getMessage()
            );
        }
    }


    public function edit()
    {
        $conn = SELF::start();
        try {
            $jsonData = file_get_contents("php://input");
            $decodedData = json_decode($jsonData);

            $username = $decodedData->username ?? null;
            $firstName = $decodedData->firstName ?? null;
            $lastName = $decodedData->lastName ?? null;
            $email = $decodedData->email ?? null;
            $birthDate = $decodedData->birthDate ?? null;
            $basicSalary = $decodedData->basicSalary ?? null;
            $group_id = $decodedData->groupID ?? null;
            $description = $decodedData->description ?? null;

            $sqlInsert = "UPDATE ms_employee SET 
                                username = '$username', 
                                firstName = '$firstName', 
                                lastName = '$lastName', 
                                email = '$email', 
                                birthDate = '$birthDate', 
                                basicSalary = '$basicSalary', 
                                group_id = '$group_id', 
                                description = '$description' 
                            WHERE (username = '$username')";
            $resultInsert = $conn->query($sqlInsert);
            if (!$resultInsert) {
                throw new Exception("Error: " . $conn->error);
            }
            $conn->close();
            return array(
                'state' => true,
                'message' => 'Update data Ok'
            );
        } catch (Exception $ex) {
            $conn->close();
            return array(
                'state' => false,
                'message' => $ex->getMessage()
            );
        }
    }


    public function softDeleteOrRestore()
    {
        $conn = SELF::start();
        try {
            $jsonData = file_get_contents("php://input");
            $decodedData = json_decode($jsonData);

            $username = $decodedData->username ?? null;;
            $status = $decodedData->status ?? null;

            $sqlInsert = "UPDATE ms_employee SET  
                                status = '$status' 
                            WHERE (username = '$username')";
            $resultInsert = $conn->query($sqlInsert);
            if (!$resultInsert) {
                throw new Exception("Error: " . $conn->error);
            }
            $conn->close();
            return array(
                'state' => true,
                'message' => 'Action Ok'
            );
        } catch (Exception $ex) {
            $conn->close();
            return array(
                'state' => false,
                'message' => $ex->getMessage()
            );
        }
    }
}
