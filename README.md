## Profile

<h1 align="center">Hi 👋, I'm Sandika Gusti Prakasa</h1>
<h3 align="center">Software engineer with 5+ years experience build & maintenance Web Application. Skilled in Frontend design, developing in multiple web-based applications incorporating a range of technologies.</h3>

- 🔭 I’m currently working on PT. ESB and successfully implement my extra ordinary project **Starbucks Indonesia Point of Sales** in all outlete around Indonesia

<h3 align="left">Connect with me:</h3>
<p align="left">
<a href="https://linkedin.com/in/sandikagustiprakasa" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="sandikagustiprakasa" height="30" width="40" /></a>
<a href="https://fb.com/sandikagustiprakasa" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/facebook.svg" alt="sandikagustiprakasa" height="30" width="40" /></a>
<a href="https://instagram.com/sandika.prakasa" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/instagram.svg" alt="sandika.prakasa" height="30" width="40" /></a>
</p>

<h3 align="left">Getting Started in Windows</h3>

<p align="left">Install Node min <b>v16.14.0</b> on your computer</p>
<p align="left">or you can download <a href="https://github.com/nvm-sh/nvm" target="_blank">NVM</a> for having multiple Node on your computer</p>

<p align="left">After pulling this repo, you need run command :</p>

```
npm install
```
<p align="left">Download <b>MYSQL</b> database <a href="https://drive.google.com/file/d/1bSazDU_707_p8Evas_LhIMbQzy4uTL19/view?usp=sharing" target="_blank">Here</a>, In my case, i use <b>MYSQL 5.7.x</b></p>

<h3 align="left">API</h3>

<p align="left">This project build from <b>PHP Native V.7.2.x</b> Dummy API, you can set Database name and Host, User, Password in "<i>{Dircetory}\api\config\connection.php</i>"</p>

<p align="left">Go to API folder directory <i>'{Dircetory}\api'</i>, and write command, example : </p>

```
php -S localhost:8181
```
<p align="left">now, your php host ready <b>http://localhost:8181</b></p>

<p align="left">I already provide <b>Postman</b> documentation for API end Point, you can get it <a href="https://drive.google.com/file/d/1nb74DyqRF2QI3SyesuA-56RcejIrSag4/view?usp=sharing" target="_blank">Here</a>, so you now can open Sign In section in Postman then try Sign In with this credential : </p>

```
Username: sandika.prakasa
Password: abc123 
```

<h3 align="left">Environtment Angular</h3>

<p align="left">Open env file in '<i>{directory}\src\env\environment.ts</i>' you can adjust <b>apiUrl</b> base on your API Host setting above</p>

<p align="left">After all setting ready, you can write command in Base Project directory and run : </p>

```
npm start
```

<p align="left">Project ready to use!</p>


<h3 align="left">IF YOU HAVING TROUBLE WITH INSTALLATION</h3>
<p align="left">Download build project <a href="https://drive.google.com/file/d/1e4RIQj2kyGhk3QyzlMwVKqpNBYrv4bD-/view?usp=sharing" target="_blank">Here</a>, but you have to set the API as per the instructions above</p>
